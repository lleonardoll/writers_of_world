Rails.application.routes.draw do
  devise_for :users
	root "welcome#index"

	resources :historys
	get "/chapters/new/:id", to: "chapters#new", as: :new_chapter
	resources :chapters

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
