class History < ApplicationRecord
	has_many :chapters

	validates_presence_of :name, :synopsi, :img_link
end
