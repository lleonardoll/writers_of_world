module ApplicationHelper

	def nav_section
		if current_user
			render "layouts/with_user"
		else
			render "layouts/without_user"
		end
	end

	def btns_index_history
		if current_user
			if current_user.id <= 3
				render "btns_index_history"
			end
		end
	end

	def btns_show_history
		if current_user
			if current_user.id <= 3
				render "btns_show_history"
			end
		end
	end

	def btns_show_chapter
		if current_user
			if current_user.id <= 3
				render "btns_show_chapter"
			end
		end
	end

end
