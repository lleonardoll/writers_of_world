class HistorysController < ApplicationController

	def index
		@historys = History.all.reverse
	end

	def new
		unless current_user
			redirect_to historys_path
		else
			unless current_user.id <= 3
				redirect_to historys_path
			end
		end

		@history = History.new
	end

	def create
		@history = History.new(history_params)

		if @history.save
			redirect_to history_path(@history.id)
		else
			render :new
		end
	end

	def edit
		unless current_user
			redirect_to historys_path
		else
			unless current_user.id <= 3
				redirect_to historys_path
			end
		end
		
		@history = History.find params[:id]
	end

	def update
		@history = History.find params[:id]

		if @history.update_attributes(history_params)
			redirect_to history_path(@history.id)
		else
			render :edit
		end
	end

	def show
		@history = History.find params[:id]
	end

	def destroy
		@history = History.find params[:id]

		@history.destroy
		redirect_to historys_path
	end

	private 
	def history_params
		params.require(:history).permit(:name, :synopsi, :img_link, :by)
	end

end