class ChaptersController < ApplicationController

	def index
		@chapters = Chapter.all.reverse		
	end

	def new
		@history = History.find params[:id]
		@chapter = Chapter.new
	end

	def create
		@chapter = Chapter.new(chapter_params)

		if @chapter.save
			redirect_to chapter_path(@chapter.id)
		else
			render :new
		end
	end

	def edit
		@chapter = Chapter.find params[:id]
	end

	def update
		@chapter = Chapter.find params[:id]

		if @chapter.update_attributes(chapter_params)
			redirect_to chapter_path(@chapter.id)
		else
			render :edit
		end
	end

	def destroy
		@chapter = Chapter.find params[:id]
		@history_id = @chapter.history_id

		@chapter.destroy
		redirect_to history_path(@history_id)		
	end

	def show
		@chapter = Chapter.find params[:id]
	end

	private
	def chapter_params
		params.require(:chapter).permit(:number, :name, :context, :background_link, :history_id);
	end

end