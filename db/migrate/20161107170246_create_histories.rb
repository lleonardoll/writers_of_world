class CreateHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :histories do |t|
      t.string :name
      t.string :synopsi
      t.string :img_link
      t.string :by

      t.timestamps
    end
  end
end
