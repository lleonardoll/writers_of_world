class CreateChapters < ActiveRecord::Migration[5.0]
  def change
    create_table :chapters do |t|
      t.integer :number
      t.string :name
      t.text :context
      t.string :background_link
      t.integer :history_id

      t.timestamps
    end
  end
end
